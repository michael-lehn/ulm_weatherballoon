//
// Created by Falko Alrik Schmidt on 09.09.23.
//

#include "Ulm_SDStorage.h"

// ------------------------------------------------------------------------------------------------------------
// (1) SPI CS PIN OF SD CARD
// ------------------------------------------------------------------------------------------------------------
Ulm_SDStorage::SDDetectPinBuilder &Ulm_SDStorage::Ulm_SDStorage_Builder::atPin(uint8_t pin) {
    this->csPin = pin;
    return *this;
}

// ------------------------------------------------------------------------------------------------------------
// (2) DIRECTORY / PATH
// ------------------------------------------------------------------------------------------------------------
Ulm_SDStorage::DataFileBuilder &Ulm_SDStorage::Ulm_SDStorage_Builder::atDirectory(const String &dir) {
    this->directory = (dir.startsWith("/") ? (dir) : ("/" + dir));
    return *this;
}

// ------------------------------------------------------------------------------------------------------------
// (3) DATA FILE
// ------------------------------------------------------------------------------------------------------------
Ulm_SDStorage::OptionalArgBuilder&
Ulm_SDStorage::Ulm_SDStorage_Builder::withDataFile(String dataFilePrefix, Ulm_SDStorage::FileType fileType) {
    this->dataFile = new Ulm_SDStorage::Ulm_SDStorage_File(dataFilePrefix, fileType);
    return *this;
}

// ------------------------------------------------------------------------------------------------------------
// (4) OPTIONAL PARAMETERS
// ------------------------------------------------------------------------------------------------------------
Ulm_SDStorage::OptionalArgBuilder&
Ulm_SDStorage::Ulm_SDStorage_Builder::withRedundancyFile(String prefix, Ulm_SDStorage::FileType fileType) {
    this->redundancyFile = new Ulm_SDStorage_File(prefix, fileType);
    return *this;
}

Ulm_SDStorage::OptionalArgBuilder&
Ulm_SDStorage::Ulm_SDStorage_Builder::withLogicForExtremeEnvironments(bool extreme) {
    this->extremeEnvironmentLogic = extreme;
    return *this;
}

Ulm_SDStorage::OptionalArgBuilder&
Ulm_SDStorage::Ulm_SDStorage_Builder::saveSDInformationOnStart(bool sd) {
    this->storeInfoSD = sd;
    return *this;
}


Ulm_SDStorage Ulm_SDStorage::Ulm_SDStorage_Builder::build() {
    return {
            this->csPin,
            this->detectPin,
            this->directory,
            this->dataFile,
            this->redundancyFile,
            this->extremeEnvironmentLogic,
            this->storeInfoSD,
    };
}



// ************************************************************************************************************
// ************************************************************************************************************
// ************************************************************************************************************
// ************************************************************************************************************
// ************************************************************************************************************
Ulm_SDStorage::Ulm_SDStorage_Builder::Ulm_SDStorage_Builder() {
    this->csPin = 10; //TODO is there a macro?
    this->directory = "/";
    this->dataFile = nullptr;
    this->redundancyFile = nullptr;
    this->extremeEnvironmentLogic = false;
    this->storeInfoSD = false;
}

Ulm_SDStorage::Ulm_SDStorage_Builder::~Ulm_SDStorage_Builder() {
    delete this->dataFile;
    delete this->redundancyFile;
}

Ulm_SDStorage::DirectoryBuilder& Ulm_SDStorage::Ulm_SDStorage_Builder::withDetectPin(int8_t detectPin) {
    this->detectPin = detectPin;
    return *this;
}


Ulm_SDStorage::Ulm_SDStorage_Builder Ulm_SDStorage::getBuilder() {
    return Ulm_SDStorage::Ulm_SDStorage_Builder();
}

Ulm_SDStorage::Ulm_SDStorage(uint8_t pin, int8_t detectPin, String directory, Ulm_SDStorage::Ulm_SDStorage_File *dataFile,
                             Ulm_SDStorage::Ulm_SDStorage_File *redundancyFile, bool extremeEnvironmentLogic,
                             bool storeInfoSd) {
    this->csPin = pin;
    this->detectPin = detectPin;
    this->directory = std::move(directory);
    this->dataFileName = Ulm_SDStorage::determineFileName(this->directory, dataFile); //TODO check
    this->redundancyFileName = Ulm_SDStorage::determineFileName(this->directory, redundancyFile); //TODO check
    this->extremeEnvironmentLogic = extremeEnvironmentLogic;
    this->storeInfoSD = storeInfoSd;
}

bool Ulm_SDStorage::store(const String& s) {
    return this->store(s.c_str());
}

bool Ulm_SDStorage::store(const char *str) {

    const size_t dataLength = strlen(str);

    // If this code will be used in extreme conditions, then we will start
    // SD on EVERY write access.
    if(this->extremeEnvironmentLogic) {
        if (!SD.begin(this->csPin)) {
            return false;
        }
    }

    // Write to primary file (data file)
    File f = SD.open(this->dataFileName, FILE_WRITE);
    if(!f) {
        return false;
    }
    bool success = (f.print(str) == dataLength);
    f.close();

    // Write to redundancy file, if it is used.
    if(this->redundancyFileName.length() > 0) { //TODO add this check again!!!
        File f = SD.open("TODO FILE NAME", FILE_WRITE);
        if(!f) {
            return false;
        }
        success &= (f.print(str) == dataLength);
        f.close();
    }

    // Finally end SD card access if used in extreme conditions.
    if(this->extremeEnvironmentLogic) {
        SD.end();
    }

    return success;
}

bool Ulm_SDStorage::begin() const {

    if(this->detectPin >= 0) {
        // Init detect pin and read it out if available.
        pinMode(this->detectPin, INPUT_PULLUP);
        // If pin is low, then SD is inserted.
        if(digitalRead(this->detectPin) == HIGH) {
            // No SD inserted -> error
            return false;
        }
    }

    if(this->storeInfoSD) {
        Sd2Card card;
        SdVolume volume;

        if (!card.init(SPI_HALF_SPEED, this->csPin)) {
            return false;
        }

        if (!volume.init(card)) {
            return false;
        }

        // Start SD
        // Store
        // and close if for extreme!
        // Open file
        //File systemInfoFile = nullptr;

        // TODO fetch all data and format them nicely!
    }


    if(!this->extremeEnvironmentLogic) {
        // If used in normal conditions, then we will start SD here.
        // Otherwise, we will start on every write and can therefore
        // ignore it here.
        return SD.begin(this->csPin);
    }
    return true;
}

String Ulm_SDStorage::determineFileName(const String& directory, Ulm_SDStorage::Ulm_SDStorage_File *file) {

    for(uint8_t i = 0; i < 1000; i++) {
        String fileName = "";
        fileName += (directory.startsWith("/") ? directory : ("/" + directory));
        fileName += '/';
        fileName += file->getPrefix();

        if(i < 10)  fileName += '0';
        if(i < 100) fileName += '0';
        fileName += i;
        fileName += ".";

        switch (file->getType()) {
            case CSV:
                fileName += "csv";
                break;
            case LOG:
                fileName += "log";
                break;
            default:
                // TXT and any other case
                fileName += "txt";
                break;
        }

        if(!SD.exists(fileName)) {
            return fileName;
        }
    }
    return{};
}


Ulm_SDStorage::Ulm_SDStorage_File::Ulm_SDStorage_File(String prefix, Ulm_SDStorage::FileType type)
        : prefix(std::move(prefix)), type(type) {
    // Nothing to do here.
}

const String &Ulm_SDStorage::Ulm_SDStorage_File::getPrefix() const {
    return prefix;
}

Ulm_SDStorage::FileType Ulm_SDStorage::Ulm_SDStorage_File::getType() const {
    return type;
}
