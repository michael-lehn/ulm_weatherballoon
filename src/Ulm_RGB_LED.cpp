//
// Created by Falko Alrik Schmidt on 19.09.23.
//

#include "Ulm_RGB_LED.h"

Ulm_RGB_LED::Ulm_RGB_LED(int16_t pin) : Adafruit_NeoPixel(1, pin, NEO_GRB + NEO_KHZ800) {
    // Nothing more to do here.
}

void Ulm_RGB_LED::off() {
    this->showColor(Ulm_RGB_LED::Color(0, 0, 0));
}

void Ulm_RGB_LED::showError() {
    this->showColor(Ulm_RGB_LED::Color(255, 0, 0));
}

void Ulm_RGB_LED::showSuccess() {
    this->showColor(Ulm_RGB_LED::Color(0, 255, 0));
}

void Ulm_RGB_LED::showWarning() {
    this->showColor(Ulm_RGB_LED::Color(255, 40, 0));
}

void Ulm_RGB_LED::showColor(uint32_t color) {
    this->setPixelColor(0, color);
    this->show();
}

bool Ulm_RGB_LED::begin() {
    Adafruit_NeoPixel::begin();
    this->clear();
    this->show();
    return true;
}
