//
// Created by Falko Alrik Schmidt on 19.09.23.
//

#ifndef UULM_WEATHERBALOON_ARDUINO_FRAMEWORK_TEST_ULM_TEMPERATURESENSOR_DS18B20_H
#define UULM_WEATHERBALOON_ARDUINO_FRAMEWORK_TEST_ULM_TEMPERATURESENSOR_DS18B20_H


#include "DallasTemperature.h"

/**
 * This class represents temperature sensors of type DS18B20.
 *
 * @author Falko Schmidt
 * @since 1.0
 */
class Ulm_TemperatureSensor_DS18B20 : public DallasTemperature {

private:
    /**
     * The OneWire interface handle, that is used to communicate
     * with the temperature sensor of type 'DS18B20'.
     */
    OneWire oneWire;

public:
    /**
     * Constructor.
     * @param pin the pin, which the sensor is connected to.
     */
    explicit Ulm_TemperatureSensor_DS18B20(uint8_t pin);

    /**
     * Reads out the sensor and returns the current temperature.
     * @return the current temperature in Celsius.
     */
    float getTemperature();
};


#endif //UULM_WEATHERBALOON_ARDUINO_FRAMEWORK_TEST_ULM_TEMPERATURESENSOR_DS18B20_H
