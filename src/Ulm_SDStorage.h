//
// Created by Falko Alrik Schmidt on 09.09.23.
//

#ifndef UULM_WEATHERBALOON_ARDUINO_FRAMEWORK_TEST_ULM_SDSTORAGE_H
#define UULM_WEATHERBALOON_ARDUINO_FRAMEWORK_TEST_ULM_SDSTORAGE_H

#include <Arduino.h>
#include <SD.h>

class Ulm_SDStorage : public SDClass {

public:
    enum FileType {
        CSV,
        LOG,
        TXT
    };

public:
    class Ulm_SDStorage_File;
    class SDChipSelectPinBuilder;
    class SDDetectPinBuilder;
    class DirectoryBuilder;
    class DataFileBuilder;
    class OptionalArgBuilder;
    class Ulm_SDStorage_Builder;

private:
    uint8_t csPin;
    int8_t detectPin;
    String directory;

    String dataFileName;
    String redundancyFileName;

    bool extremeEnvironmentLogic;
    bool storeInfoSD;

private:
    Ulm_SDStorage(uint8_t csPin, int8_t detectPin, String directory, Ulm_SDStorage_File* dataFile,
                  Ulm_SDStorage_File* redundancyFile, bool extremeEnvironmentLogic,
                  bool storeInfoSd);

private:
    static String determineFileName(const String &directory, Ulm_SDStorage_File *file);

public:
    static Ulm_SDStorage::Ulm_SDStorage_Builder getBuilder();

    bool begin() const;

    bool store(const String& s);
    bool store(const char* str);

};

// ------------------------------------------------------------------------------------------------------------
// FIRST STEP - MANDATORY - SET CS PIN OF SD CARD
// ------------------------------------------------------------------------------------------------------------
class Ulm_SDStorage::SDChipSelectPinBuilder {
protected:
    uint8_t csPin = 10; //TODO is there a macro?

public:
    virtual Ulm_SDStorage::SDDetectPinBuilder& atPin(uint8_t pin) = 0;
};


class Ulm_SDStorage::SDDetectPinBuilder {
protected:
    int8_t detectPin = -1;

public:
    virtual Ulm_SDStorage::DirectoryBuilder& withDetectPin(int8_t detectPin) = 0;
};

// ------------------------------------------------------------------------------------------------------------
// SECOND STEP - OPTIONAL - SET DIRECTORY FOR DATA LOGGING
// ------------------------------------------------------------------------------------------------------------
class Ulm_SDStorage::DirectoryBuilder {
protected:
    String directory;

public:
    virtual DataFileBuilder& atDirectory(const String& dir) = 0;
};


// ------------------------------------------------------------------------------------------------------------
// THIRD STEP - OPTIONAL - SET DIRECTORY FOR DATA LOGGING
// ------------------------------------------------------------------------------------------------------------
class Ulm_SDStorage::DataFileBuilder {
protected:
    Ulm_SDStorage::Ulm_SDStorage_File* dataFile = nullptr;

public:
    virtual OptionalArgBuilder& withDataFile(String dataFilePrefix, enum FileType fileType) = 0;
};


class Ulm_SDStorage::OptionalArgBuilder {
protected:
    Ulm_SDStorage::Ulm_SDStorage_File* redundancyFile = nullptr;
    bool extremeEnvironmentLogic = false;
    bool storeInfoSD = false;

public:
    virtual Ulm_SDStorage::OptionalArgBuilder& withRedundancyFile(String prefix, enum FileType fileType) = 0;
    virtual Ulm_SDStorage::OptionalArgBuilder& withLogicForExtremeEnvironments(bool extreme = true) = 0;
    virtual Ulm_SDStorage::OptionalArgBuilder& saveSDInformationOnStart(bool sd = true) = 0;

    virtual Ulm_SDStorage build() = 0;

};


class Ulm_SDStorage::Ulm_SDStorage_Builder : public SDChipSelectPinBuilder, public SDDetectPinBuilder,
                                             public DirectoryBuilder, public DataFileBuilder,
                                             public OptionalArgBuilder {

public:
    explicit Ulm_SDStorage_Builder();

    SDDetectPinBuilder &atPin(uint8_t pin) override;

    DirectoryBuilder& withDetectPin(int8_t detectPin) override;

    DataFileBuilder &atDirectory(const String &dir) override;
    OptionalArgBuilder &withDataFile(String dataFilePrefix, enum FileType fileType) override;

    OptionalArgBuilder &withRedundancyFile(String prefix, enum FileType fileType) override;
    OptionalArgBuilder &withLogicForExtremeEnvironments(bool extreme) override;
    OptionalArgBuilder &saveSDInformationOnStart(bool sd) override;

    Ulm_SDStorage build() override;

    virtual ~Ulm_SDStorage_Builder();
};


class Ulm_SDStorage::Ulm_SDStorage_File {
private:
    String prefix;
    enum FileType type;

public:
    Ulm_SDStorage_File(String prefix, FileType type);

    [[nodiscard]] const String &getPrefix() const;
    [[nodiscard]] FileType getType() const;
};

#endif //UULM_WEATHERBALOON_ARDUINO_FRAMEWORK_TEST_ULM_SDSTORAGE_H
