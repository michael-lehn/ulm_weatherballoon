//
// Created by Falko Alrik Schmidt on 19.09.23.
//

#include "Ulm_TemperatureSensor_DS18B20.h"

Ulm_TemperatureSensor_DS18B20::Ulm_TemperatureSensor_DS18B20(uint8_t pin) : DallasTemperature() {
    this->oneWire = OneWire(pin);       // This is to not use heap.
    this->setOneWire(&(this->oneWire)); // This is to not use heap.
    this->setResolution(12);            // Highest resolution. Conversion time = 750 ms max.
    this->setWaitForConversion(false);  // Async mode (non-blocking).
}

float Ulm_TemperatureSensor_DS18B20::getTemperature() {
    float t = this->getTempCByIndex(0);     // Read the current temperature.
    this->requestTemperaturesByIndex(0);    // Ask sensor to read a new temperature.
    return t;
}
