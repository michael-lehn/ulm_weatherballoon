//
// Created by Falko Alrik Schmidt on 11.10.23.
//

#ifndef UULM_WEATHERBALOON_ARDUINO_FRAMEWORK_TEST_ULM_LED_H
#define UULM_WEATHERBALOON_ARDUINO_FRAMEWORK_TEST_ULM_LED_H


#include "Ulm_Beginnable.h"

/**
 * This class represents a simple LED.
 *
 * @author Falko Schmidt
 * @since 1.0
 */
class Ulm_LED : public Ulm_Beginnable {

public:
    enum Mode {
        ACTIVE_LOW,
        ACTIVE_HIGH
    };

private:
    /**
     * The physical pin, which the LED is connected to.
     */
    const uint8_t pin;

    /**
     * The activation mode if this LED. Either {@code ACTIVE_HIGH} or {@code ACTIVE_LOW}.
     */
    const enum Mode mode;

public:
    /**
     * Default constructor to create an object of type Ulm_LED.
     * @param pin the physical pin, which the LED is connected to.
     * @param mode optional argument to configure the activation mode of the LED. This
     * can either be {@code ACTIVE_HIGH}, meaning that the LED will turn on, if pin is set
     * to {@code HIGH}, or {@code ACTIVE_LOW}, meaning that the LED will turn on, if pin is
     * set to {@code LOW}. This value defaults to {@code ACTIVE_HIGH}.
     */
    explicit Ulm_LED(uint8_t pin, Mode mode = ACTIVE_HIGH);

    /**
     * Initializes LED and turns it off.
     * @return always {@code true}.
     */
    bool begin() override;

    /**
     * Turns the LED on.
     */
    void on();

    /**
     * Turns the LED off.
     */
    void off();

    /**
     * Toggles the LED. If the LED was on before, then calling this method will
     * turn off the LED, otherwise the LED will be turned on.
     */
    void toggle() const;
};


#endif //UULM_WEATHERBALOON_ARDUINO_FRAMEWORK_TEST_ULM_LED_H
