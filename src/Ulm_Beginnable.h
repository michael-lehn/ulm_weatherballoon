//
// Created by Falko Alrik Schmidt on 11.10.23.
//

#ifndef UULM_WEATHERBALOON_ARDUINO_FRAMEWORK_TEST_ULM_BEGINNABLE_H
#define UULM_WEATHERBALOON_ARDUINO_FRAMEWORK_TEST_ULM_BEGINNABLE_H

/**
 * All electrical components need some sort of initialisation process
 * after the microcontroller receives power. Sadly, the arduino framework
 * does not provide a common name for an init-method and so there are multiple
 * wild names in libraries, like {@code begin()}, {@code init()}, {@code start()}
 * or others.
 *
 * To at least provide a uniform name within this library, all components will
 * inherit this class and therefore must implement the virtual method {@code begin(...)}.
 */
class Ulm_Beginnable {

public:
    /**
     * Initializes the component.
     * @return {@code true} on, and only on, success without ANY error, {@code false} otherwise.
     */
    virtual bool begin() = 0;

};


#endif //UULM_WEATHERBALOON_ARDUINO_FRAMEWORK_TEST_ULM_BEGINNABLE_H
