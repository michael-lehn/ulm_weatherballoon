//
// Created by Falko Alrik Schmidt on 26.09.23.
//

#ifndef UULM_WEATHERBALOON_ARDUINO_FRAMEWORK_TEST_ULM_MS5607_H
#define UULM_WEATHERBALOON_ARDUINO_FRAMEWORK_TEST_ULM_MS5607_H

#include <Wire.h>
#include "Ulm_Beginnable.h"
#include "MS5xxx.h"

/**
 * This class represents altitude sensors of type MS5607.
 *
 * @author Falko Schmidt
 * @since 1.0
 */
class Ulm_MS5607 : public MS5xxx, public Ulm_Beginnable {

public:
    /**
     * Constructor.
     * @param address the i2c address of MS5607 sensor.
     * @param aWire the i2c connection, defaults to {@code &Wire}.
     */
    explicit Ulm_MS5607(char address);

    /**
     * Initializes the sensor.
     * @return {@code true} on success, {@code false} otherwise.
     */
    bool begin() override;
};


#endif //UULM_WEATHERBALOON_ARDUINO_FRAMEWORK_TEST_ULM_MS5607_H
