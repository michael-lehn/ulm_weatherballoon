//
// Created by Falko Alrik Schmidt on 11.10.23.
//

#include <Arduino.h>
#include "Ulm_LED_BuiltIn.h"

Ulm_LED_BuiltIn::Ulm_LED_BuiltIn() : Ulm_LED(LED_BUILTIN, ACTIVE_HIGH) {
    // Nothing more to do here.
}
