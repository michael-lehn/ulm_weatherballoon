//
// Created by Falko Alrik Schmidt on 26.09.23.
//

#include "Ulm_MS5607.h"

Ulm_MS5607::Ulm_MS5607(char address) : MS5xxx(&Wire) {
    this->setI2Caddr(address);
}


bool Ulm_MS5607::begin() {
    return this->connect() == 0;
}
