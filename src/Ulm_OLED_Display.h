//
// Created by Falko Alrik Schmidt on 19.09.23.
//

#ifndef UULM_WEATHERBALOON_ARDUINO_FRAMEWORK_TEST_ULM_OLED_DISPLAY_H
#define UULM_WEATHERBALOON_ARDUINO_FRAMEWORK_TEST_ULM_OLED_DISPLAY_H

// This is used, to not show the splash screen
// implemented in the used SSD1306 library.
#define SSD1306_NO_SPLASH

#include "Adafruit_SSD1306.h"
#include "Ulm_Beginnable.h"

/**
 * This class represents an OLED with dimensions of 128 x 64 pixels
 * which is connected via i2c at address 0x3C.
 *
 * @author Falko Schmidt
 * @since 1.0
 */
class Ulm_OLED_Display : public Adafruit_SSD1306, public Ulm_Beginnable {

public:
    /**
     * Default constructor.
     */
    Ulm_OLED_Display();

    /**
     * Initializes the display.
     * After calling this method, the display can be written to.
     * @return {@code true} on success, {@code false} otherwise.
     */
    bool begin() override;
};


#endif //UULM_WEATHERBALOON_ARDUINO_FRAMEWORK_TEST_ULM_OLED_DISPLAY_H
