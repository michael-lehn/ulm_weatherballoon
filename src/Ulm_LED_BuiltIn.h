//
// Created by Falko Alrik Schmidt on 11.10.23.
//

#ifndef UULM_WEATHERBALOON_ARDUINO_FRAMEWORK_TEST_ULM_LED_BUILTIN_H
#define UULM_WEATHERBALOON_ARDUINO_FRAMEWORK_TEST_ULM_LED_BUILTIN_H

#include "Ulm_LED.h"

/**
 * This class represents the built-in LED of an Arduino.
 *
 * @since 1.0
 * @author Falko Schmidt
 */
class Ulm_LED_BuiltIn : public Ulm_LED {

public:
    /**
     * Default constructor. This will construct an object of type
     * {@code Ulm_LED} at pin {@code LED_BUILTIN}.
     */
    Ulm_LED_BuiltIn();

};


#endif //UULM_WEATHERBALOON_ARDUINO_FRAMEWORK_TEST_ULM_LED_BUILTIN_H
