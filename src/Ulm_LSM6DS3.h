//
// Created by Falko Alrik Schmidt on 26.09.23.
//

#ifndef UULM_WEATHERBALOON_ARDUINO_FRAMEWORK_TEST_ULM_LSM6DS3_H
#define UULM_WEATHERBALOON_ARDUINO_FRAMEWORK_TEST_ULM_LSM6DS3_H

#include <Wire.h>
#include "Arduino_LSM6DS3.h"
#include "Ulm_Beginnable.h"

/**
 * This class represents IMUs of type LSM6DS3.
 *
 * @author Falko Schmidt
 * @since 1.0
 */
class Ulm_LSM6DS3 : public LSM6DS3Class, public Ulm_Beginnable {

public:
    /**
     * Constructor.
     * @param address the i2c address of the LSM6DS3 sensor.
     * @param wire the i2c interface.
     */
    explicit Ulm_LSM6DS3(uint8_t address, TwoWire &wire = Wire);

    /**
     * Inits the sensor.
     * @return {@code true} on success, {@code false} otherwise.
     */
    bool begin() override;
};


#endif //UULM_WEATHERBALOON_ARDUINO_FRAMEWORK_TEST_ULM_LSM6DS3_H
