//
// Created by Falko Alrik Schmidt on 19.09.23.
//

#ifndef UULM_WEATHERBALOON_ARDUINO_FRAMEWORK_TEST_ULM_RGB_LED_H
#define UULM_WEATHERBALOON_ARDUINO_FRAMEWORK_TEST_ULM_RGB_LED_H


#include "Adafruit_NeoPixel.h"
#include "Ulm_Beginnable.h"

/**
 * This class represents an RGB LED of type WS2812, WS2813 or similar.
 *
 * @author Falko Schmidt
 * @since 1.0
 */
class Ulm_RGB_LED : public Adafruit_NeoPixel, public Ulm_Beginnable {

public:
    /**
     * Default constructor.
     * @param pin the pin, which the RGB LED is connected to.
     */
    explicit Ulm_RGB_LED(int16_t pin);

    /**
     * Inits the RGB LED and turns it off.
     * @return always {@code true}.
     */
    bool begin() override;

    /**
     * Turns the RGB LED off.
     */
    void off();

    /**
     * Shows a green color on the RGB LED.
     */
    void showSuccess();

    /**
     * Shows a red color on the RGB LED.
     */
    void showError();

    /**
     * Shows a yellow color on the RGB LED.
     */
    void showWarning();

private:
    /**
     * Shows any RGB color on the LED.
     * This method will be used only within this class.
     * @param color the color to show.
     */
    void showColor(uint32_t color);

};


#endif //UULM_WEATHERBALOON_ARDUINO_FRAMEWORK_TEST_ULM_RGB_LED_H
