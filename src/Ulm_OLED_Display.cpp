//
// Created by Falko Alrik Schmidt on 19.09.23.
//

#include "Ulm_OLED_Display.h"

Ulm_OLED_Display::Ulm_OLED_Display() : Adafruit_SSD1306(128, 64) {}

bool Ulm_OLED_Display::begin() {
    return Adafruit_SSD1306::begin(SSD1306_SWITCHCAPVCC, 0x3C);
}
