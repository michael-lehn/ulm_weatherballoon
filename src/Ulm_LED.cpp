//
// Created by Falko Alrik Schmidt on 11.10.23.
//

#include <Arduino.h>
#include "Ulm_LED.h"

Ulm_LED::Ulm_LED(uint8_t pin, Ulm_LED::Mode mode) : pin(pin), mode(mode) {
    // Nothing more to do here.
}

bool Ulm_LED::begin() {
    pinMode(this->pin, OUTPUT);
    this->off();
    return true;
}

void Ulm_LED::on() {
    digitalWrite(this->pin, this->mode == ACTIVE_HIGH ? HIGH : LOW);
}

void Ulm_LED::off() {
    digitalWrite(this->pin, this->mode == ACTIVE_HIGH ? LOW : HIGH);
}

void Ulm_LED::toggle() const {
    // Use digitalRead to fetch the current pin value, then invert it and write back.
    digitalWrite(this->pin, digitalRead(this->pin) == HIGH ? LOW : HIGH);
}
