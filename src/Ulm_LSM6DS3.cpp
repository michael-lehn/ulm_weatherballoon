//
// Created by Falko Alrik Schmidt on 26.09.23.
//
#include "Ulm_LSM6DS3.h"

Ulm_LSM6DS3::Ulm_LSM6DS3(uint8_t address, TwoWire &wire) : LSM6DS3Class(wire, address) {
    // Nothing more to do here.
}

bool Ulm_LSM6DS3::begin() {
    return LSM6DS3Class::begin();
}
