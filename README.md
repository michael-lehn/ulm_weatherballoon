# Ulm_Weatherballoon

This repository is a library implemented for the Arduino Framework.
It is used for the implementation of dataloggers for use in high-altitude balloons.

Falko Schmidt, 2023

# Supported electrical components

### Sensors

---

The list below shows all sensors, that are supported by this library.
Please note, that this library is only a lightweight addition to the Arduino framework
to hide some nast details, that might be confusing for students, meaning that many implementations
are defined in other libraries which this depends on.

<table>
    <thead>
        <tr>
            <th>Name of sensor</th>
            <th>Type</th>
            <th>Link (Mouser)</th>
            <th>Link (DigiKey)</th>
            <th>Link (JLCPCB)</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>DS18B20</td>
            <td>Temperature sensor</td>
            <td><a href="https://www.mouser.de/ProductDetail/Analog-Devices-Maxim-Integrated/DS18B20%2b?qs=7H2Jq%252ByxpJKegCKabDbglA%3D%3D">Part at Mouser</a></td>
            <td><a href="https://www.digikey.de/de/products/detail/umw/DS18B20/16705963">Part at DigiKey</a></td>
            <td><a href="https://jlcpcb.com/partdetail/348632-DS18B20/C376006">Part at JLCPCB</a></td>
        </tr>
        <tr>
            <td>LSM6DS3</td>
            <td>Inertial measurement unit (IMU)</td>
            <td><a href="https://www.mouser.de/ProductDetail/STMicroelectronics/LSM6DS3TR-C?qs=u4fy%2FsgLU9NHwPlhP3LTBw%3D%3D">Part at Mouser</a></td>
            <td><a href="https://www.digikey.de/de/products/detail/stmicroelectronics/LSM6DS3HTR/5872322">Part at DigiKey</a></td>
            <td><a href="https://jlcpcb.com/partdetail/Stmicroelectronics-LSM6DS3TR/C95230">Part at JLCPCB</a></td>
        </tr>
        <tr>
            <td>MS5607</td>
            <td>Altimeter</td>
            <td><a href="https://www.mouser.de/ProductDetail/Measurement-Specialties/MS560702BA03-50?qs=urSpXqmdEVImCZG1H%252BT8ng%3D%3D">Part at Mouser</a></td>
            <td><a href="https://www.digikey.de/de/products/detail/te-connectivity-measurement-specialties/MS560702BA03-50/4700931">Part at DigiKey</a></td>
            <td><a href="https://jlcpcb.com/partdetail/TeConnectivity-MS560702BA0350/C97627">Part at JLCPCB</a></td>
        </tr>
    </tbody>
</table>

### Actors

---

A datalogger not only consists of sensors. There also has to be some
output to the user in case of an error. For this, several actors can be used:

<table>
    <thead>
        <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Implementation (Header)</th>
            <th>Further details</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Generic LED</td>
            <td>A simple LED that is connected to a GPIO of a microcontroller.</td>
            <td><a href="src/Ulm_LED.h">Implementation</a></td>
            <td><a href="#generic-led">Details</a></td>
        </tr>
        <tr>
            <td>Built-In LED</td>
            <td>The on-board LED of an Arduino.</td>
            <td><a href="src/Ulm_LED_BuiltIn.h">Implementation</a></td>
            <td><a href="#built-in-led">Details</a></td>
        </tr>
        <tr>
            <td>RGB LED (a.k.a. NeoPixel)</td>
            <td>A single NeoPixel RGB LED connected to any GPIO of the microcontroller.</td>
            <td><a href="src/Ulm_RGB_LED.h">Implementation</a></td>
            <td><a href="#rgb-led">Details</a></td>
        </tr>
        <tr>
            <td>OLED-Display</td>
            <td>A small OLED-display that has the dimensions of 128 x 64 pixels.</td>
            <td><a href="src/Ulm_OLED_Display.h">Implementation</a></td>
            <td><a href="#oled-display">Details</a></td>
        </tr>
    </tbody>
</table>

#### Generic LED
An LED can either be ACTIVE_HIGH, meaning that it is ON, if pin is HIGH. Otherwise, it is ACTIVE_LOW, 
meaning it is ON, if pin is LOW. Please make sure to use the correct setting, otherwise your LED will 
react inverted! Check the table below to see the valid implementation and according schematic.

<table>
<thead>
<tr>
<th>Schematic</th>
<th>Minimum running software example</th>
</tr>
</thead>
<tbody>

<tr>
<td>
<img height=400px src="assets/Generic_LED_Active_High.png" alt="Generic LED Active High">
</td>
<td>
<pre>
// Create object that represents the LED.
Ulm_LED led = Ulm_LED(PIN_NUMBER);

void setup() {
    // Init the LED.
    led.begin();
}

void loop() {
    // Toggle LED on and of forever.
    led.on();
    delay(1000);
    led.off();
    delay(1000);
}
</pre>
</td>
</tr>

<tr>
<td>
<img height=400px src="assets/Generic_LED_Active_Low.png" alt="Generic LED Active Low">
</td>
<td>
<pre>
// Create object that represents the LED.
Ulm_LED led = Ulm_LED(PIN_NUMBER, Ulm_LED::Mode::ACTIVE_LOW);

void setup() {
    // Init the LED.
    led.begin();
}

void loop() {
    // Toggle LED on and of forever.
    led.on();
    delay(1000);
    led.off();
    delay(1000);
}
</pre>
</td>
</tr>
</tbody>
</table>


#### Built-in LED
Use this to control the built-in LED, which the Arduino UNO, NANO, MEGA and several others support.
You do not need to pass any pin, this will be done automatically.

<table>
<thead>
<tr>
<th>Schematic</th>
<th>Minimum running software example</th>
</tr>
</thead>
<tbody>
<tr>
<td>
Built-in LED.<br/>
No additional hardware required.
</td>
<td>
<pre>
// Create object that represents the LED.
auto led = Ulm_LED_BuiltIn();

void setup() {
    // Init the LED.
    led.begin();
}

void loop() {
    // Toggle LED on and of forever.
    led.on();
    delay(1000);
    led.off();
    delay(1000);
}
</pre>
</td>
</tr>

</tbody>
</table>


#### RGB LED
This implementation only works for a SINGLE NeoPixel. If you want to control a chain of them, then
please refer to the Adafruit NeoPixel library instead! Some predefined methods allow a single RGB LED
to be easily used as status indicator.

<ul>
<li>
<b>C1</b> is a decoupling capacitor, which can provide power if needed
(for example when changing color, dimming or similar operations)
without directly pulling current from the main power supply.
</li>
<li>
<b>R1</b> is used to prevent voltage spikes on the data line to reach the chips built into the LEDs.
To prevent this effect, please use a resistor with a value of 300Ω - 500Ω.
<a href="https://learn.adafruit.com/adafruit-neopixel-uberguide/best-practices">
Click here</a> for additional information.
</li>
<li>
<b>R2</b> is used for the same reasons. According to several experiences online,
omitting the resistor results in the LED no longer working.
</li>
<li>
Instead of the <b>WS2813</b>, also <b>WS2812</b> LEDs can be used.
</li>
</ul>


<table>
<thead>
<tr>
<th>Schematic</th>
<th>Minimum running software example</th>
</tr>
</thead>
<tbody>
<tr>
<td>
<img height="300px" src="assets/RGB_LED.png" alt="RGB LED">
</td>
<td>
<pre>
// Create object that represents the LED.
auto rgb = Ulm_RGB_LED(PIN_NUMBER);

void setup() {
    Serial.begin(9600);
    if(!rgb.begin()) {
        Serial.println("Failed to init RGB LED!");
        while(1);
    }
}

void loop() {
    rgb.showSuccess();  // Green
    delay(1000);
    rgb.showWarning();  // Yellow
    delay(1000);
    rgb.showError();    // Red
    delay(1000);
}
</pre>
</td>
</tr>
</tbody>
</table>


#### OLED-Display
This only works for the dimension of 128 x 64 pixels and the i2c address 0x3C. If your display does
not apply to these characteristics, then the implementation can not be used!

<table>
<thead>
<tr>
<th>Schematic</th>
<th>Minimum running software example</th>
</tr>
</thead>
<tbody>
<tr>
<td>
<img height=400px src="assets/OLED.png" alt="OLED">
</td>
<td>
<pre>
// Create object that represents the OLED.
auto display = Ulm_OLED_Display();

void setup() {
    if(display.begin()) {
        display.setCursor(0, 0);
        display.print("Hello!");
    }
}

void loop() {
    //...
}
</pre>
</td>
</tr>
</tbody>
</table>


### Storage modules
Of course, sampling sensors it not enough. You need to store
your read values somewhere. These implementations can be used
to store data:
<table>
    <thead>
        <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Implementation (Header)</th>
            <th>Further Details</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Ulm SD-Storage</td>
            <td>SD-Card storage with configurable auto-detect pin and optional data redundancy implementation</td>
            <td><a href="src/Ulm_SDStorage.h">Implementation</a></td>
            <td><a href="#ulm-sd-storage">Details</a></td></td>
        </tr>
    </tbody>
</table>


#### Ulm SD Storage
The Ulm SD Storage describes the hardware structure listed below. It basically consists of an SD card which will
be accessed via SPI. An optional detection pin can be used to check, if a card is inserted.

<ul>
<li>
<b>C1:</b>
This capacitor is used to buffer the input power for the +3.3V voltage regulator.
</li>
<li>
<b>C2:</b>
A small transistor connected to the BYPASS pin (BP) if the voltage regulator.
</li>
<li>
<b>C3:</b>
This capacitor is used to buffer the generated +3V3 at the output of the voltage regulator.
</li>
<li>
<b>C4 and C5:</b>
Remember the decoupling capacitor described for the RGB LEDs? Same technic applies here.
The component 'U2' can suddenly draw more current and therefore require more power. This will
be delivered from the capacitors C4 and C5 and not directly influence the main power supply.
</li>
<li>
<b>C6:</b>
SD cards require some power during reading and more during writing processes. Again a capacitor
is used to decouple the SD card from the main power source and the voltage regulator. This capacitor
must have a size of 10 uF.
</li>
<li>
<b>J1:</b>
The SD-card slot. There are several different components out there. We will use the part 'Hirose DM3D-SF',
which is very simple to use and reliable!
</li>
<li>
<b>R1:</b>
Pull-up resistor for the Chip-select / Slave-select (CS / SS) pin of the SD-card. If this pin is low, then
the SD-card will react to and process commands sent via SPI. To prevent this from accidentally happen, a
pull-up resistor is used. 10 kΩ are appropriate.
</li>
<li>
<b>R2:</b>
Pull-up resistor for the detect pin. If an SD-card is inserted, then the detect pin will be LOW. But what happens,
if no SD-card is inserted? This has to be well-defined, and therefore we will use a pull-up resistor of 10 kΩ. This
will set the pin to HIGH, if no SD-card is inserted. Please note: Other than the SS pin, which pulls up to +3.3 V,
it is necessary, that the detection pin pulls up to the voltage level of the microcontroller. In case of most
Arduinos, this is 5 V.
</li>
<li>
<b>U1:</b>
This component is a small voltage regulator, which provides an output voltage of +3.3 V for a wide input voltage
(for example +5 V). Now, if you take a close look at the Arduino, you might notice a pin labeled with '3.3 V'.
Why not just use that instead? We can not directly use the +3.3 V provided by our Arduino, because it can most likely
not provide enough current, so instead we will create our own needed voltage from the +5 V power rail, where we can
get more than enough power from!
</li>
<li>
<b>U2:</b>
Okay, we are almost done with the description of this circuit! Only this weird component 'TXB0104' left - what is that?
Remember, that our SD-card needs +3.3 V? Our Arduino uses +5 V. And these two components need to exchange data at some
point. If we would directly connect the +5 V signals from our Arduino to the SD-card, then we would break it quite fast.
Also, the other way round might be a problem: Directly connecting the +3.3 V to an Arduino might be detected as LOW
instead of HIGH. The component 'TXB0104' provides a solution to that problem: It translates the Arduino signals (+5 V)
into values, that the SD-card will understand (+3.3 V) and vice versa. These components are called logic level
converters.
</li>
</ul>

<table>
<thead>
<tr>
<th>Schematic</th>
<th>Minimum running software example</th>
</tr>
</thead>
<tbody>
<tr>
<td>
<img height=300px src="assets/SD_Storage.png" alt="SD Storage">
</td>
<td>
<pre>
// TODO
</pre>
</td>
</tr>
</tbody>
</table>


# Supported data-loggers

Below you can find a list of supported data-loggers by this library.
Several designs will be added in near future!

### SGUduino 2024 (Arduino UNO R4) [Falko Schmidt]

<img height=400px src="assets/SGUduino_2024_Top.png" alt="SGUduino 2024 (Top)">
<img height=400px src="assets/SGUduino_2024_Bottom.png" alt="SGUduino 2024 (Bottom)">

The SGUduino 2024 Datalogger was created for the Seminar NWT course, that took place 2023-2024
at Ulm University. This PCB was created to show students a way how to structure sensors in a
neat way on a PCB that can be used with an Arduino UNO.
These electrical components are used:
<ul>
<li>RGB LED of type 'WS2812'</li>
<li>RESET button</li>
<li>DS18B20 temperature sensor</li>
<li>LSM6DS3 IMU</li>
<li>microSD card with logic-level-converter</li>
<li>MS5607 altimeter</li>
<li>stemmaQT connector for further external sensors.</li>
</ul>

With the sensor set mentioned above, several different charactical values of the atmosphere
can be measured during the flight in a weather balloon:
<ul>
<li>Uptime [ms]</li>
<li>Surrounding temperature [°C]</li>
<li>Gyroscope X, Y and Z [deg/s]</li>
<li>Acceleration X, Y and Z [g]</li>
<li>Air pressure [mBar]</li>
<li>Altitude above sea level [m]</li>
</ul>

If you have this PCB and want to start programming and testing, then check out
[the ready-to-run program](examples/Ulm_Datalogger_SGUduino2024/Ulm_Datalogger_SGUduino2024.ino).
