#include <Arduino.h>                // <- Import Arduino-functions
#include <Ulm_Weatherballoon.h>     // <- Import weather-balloon components

void setup() {
    // This call will start the i2c interface of the Arduino.
    Wire.begin();

    // We will also start the serial connection and wait for it
    // to begin. After that, we will output a line of text, to
    // make sure, that Serial is running.
    Serial.begin(9600);
    while(!Serial);
    Serial.println("Running I2C scanner...");
}


void loop() {

    int amountDevices = 0;

    for(int address=/*TODO: Run this loop from address 1(inclusive) to 127(exclusive)*/) {
        Wire.beginTransmission(address);
        if(Wire.endTransmission() == 0) {
            // i2c device found.
            amountDevices = amountDevices + 1;
            Serial.print("[I2C-Scanner] Found device at address 0x");
            Serial.println(address, HEX);
        }
    }

    // TODO: Please output the amount of devices found to Serial.
    //  Please also make sure, to output something, if NO device is found!

    // TODO: After one scan, we want to terminate our program.
    //  How does this work on a microcontroller?
    //  (Hint: delay(...) is NOT an appropriate solution!)
}
