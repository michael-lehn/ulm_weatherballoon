#include <Arduino.h>                // <- Import Arduino-functions
#include <Ulm_Weatherballoon.h>     // <- Import weather-balloon components

// This can be used to control the LED.
// Please note, that you must adapt the pin, which the
// LED is connected to in your circuit.
constexpr uint8_t MY_LED_PIN = /*TODO*/;
Ulm_LED led = Ulm_LED(MY_LED_PIN);

void setup() {
    // TODO Your task here:
    //  Initialize the LED.
}


void loop() {
    // TODO Your task here:
    //  We will implement a flashing light:
    //      (1) Turn LED on for 100 milliseconds.
    //      (2) Turn LED off for 900 milliseconds.
    //      (3) Repeat.
}
