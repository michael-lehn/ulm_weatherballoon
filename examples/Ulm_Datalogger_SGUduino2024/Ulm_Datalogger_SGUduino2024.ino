#include <Arduino.h>
#include "Ulm_Weatherballoon.h"

// --------------------------------------------------------------------------------------------------------------------
// SD CARD
// --------------------------------------------------------------------------------------------------------------------
constexpr uint8_t SD_CS_PIN                             = 10;
constexpr uint8_t SD_DETECT_PIN                         = 2;

// --------------------------------------------------------------------------------------------------------------------
// RGB LED (NeoPixel)
// --------------------------------------------------------------------------------------------------------------------
constexpr uint8_t LED_RGB_PIN                           = 9;

// --------------------------------------------------------------------------------------------------------------------
// MS5607 ALTITUDE SENSOR
// --------------------------------------------------------------------------------------------------------------------
constexpr uint8_t MS5607_ALTITUDE_SENSOR_I2C_ADDRESS    = 0x77;

// --------------------------------------------------------------------------------------------------------------------
// DS18B20 TEMPERATURE SENSOR
// --------------------------------------------------------------------------------------------------------------------
constexpr uint8_t DS18B20_TEMPERATURE_SENSOR_PIN        = 6;

// --------------------------------------------------------------------------------------------------------------------
// LSM6DS3 IMU (ACCELEROMETER AND GYROSCOPE)
// --------------------------------------------------------------------------------------------------------------------
constexpr uint8_t LSM6DS3_IMU_SENSOR_I2C_ADDRESS        = 0x6A;


// ====================================================================================================================

// INPUT DEVICES (SENSORS)
auto imu                = Ulm_LSM6DS3(LSM6DS3_IMU_SENSOR_I2C_ADDRESS);
auto altitudeSensor     = Ulm_MS5607(MS5607_ALTITUDE_SENSOR_I2C_ADDRESS);
auto temperatureSensor  = Ulm_TemperatureSensor_DS18B20(DS18B20_TEMPERATURE_SENSOR_PIN);

// OUTPUT DEVICES (LED AND DISPLAY)
auto led                = Ulm_RGB_LED(LED_RGB_PIN);
auto display            = Ulm_OLED_Display();

// STORAGE DEVICE
Ulm_SDStorage storage = Ulm_SDStorage::getBuilder()
        .atPin(SD_CS_PIN)
        .withDetectPin(SD_DETECT_PIN)
        .atDirectory("SGUduino")
        .withDataFile("DATA_", Ulm_SDStorage::FileType::TXT)
        .withRedundancyFile("RED_", Ulm_SDStorage::FileType::TXT)
        .withLogicForExtremeEnvironments()
        .saveSDInformationOnStart()
        .build();


// ====================================================================================================================
// DATA REPRESENTATION
// ====================================================================================================================
// Structure representing our raw sensor data.
// All of these values will be sampled and stored to SD card.
struct SensorData {
    unsigned long   uptimeMillis;       // Uptime of the MCU (time since power on) in milliseconds
    float           temperature;        // The ambient temperature in Celsius
    float           accelerationX;      // Acceleration for all three axes (g (earth gravity))
    float           accelerationY;
    float           accelerationZ;
    float           gyroscopeX;         // Gyroscope of all three axes (degrees/second)
    float           gyroscopeY;
    float           gyroscopeZ;
    double          pressure;           // Pressure in mBar
    double          altitude;           // Altitude in meters
} sensorData;


/**
 * Storage routine for sensor data. The data will be formatted and then written to
 * SD card. After that, the onboard LED will be either green (success) or red (failure).
 */
void storeSensorData() {
    char buffer[128];
    sprintf(buffer,
            "%lu;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f",
            sensorData.uptimeMillis,
            sensorData.temperature,
            sensorData.accelerationX,
            sensorData.accelerationY,
            sensorData.accelerationZ,
            sensorData.gyroscopeX,
            sensorData.gyroscopeY,
            sensorData.gyroscopeZ,
            sensorData.pressure,
            sensorData.altitude
    );

    if(storage.store(buffer)) {
        led.showSuccess();
    } else {
        led.showError();
    }

    Serial.println(buffer);
}


// ====================================================================================================================
// SERIAL HELPER FUNCTIONS
// ====================================================================================================================
/**
 * Outputs a warning to Serial.
 * @param str the warning message.
 */
inline void printWarning(const char* str) {
    Serial.print("[WARN] ");
    Serial.println(str);
}

/**
 * Outputs an error to Serial.
 * @param str the error message.
 */
inline void printError(const char* str) {
    Serial.print("[FAIL] ");
    Serial.println(str);
}

/**
 * Outputs a success message to Serial.
 * @param str the success message.
 */
inline void printSuccess(const char* str) {
    Serial.print("[ OK ] ");
    Serial.println(str);
}

/**
 * Outputs an information to Serial.
 * @param str the information message.
 */
inline void printInformation(const char* str) {
    Serial.print("[INFO] ");
    Serial.println(str);
}


// ====================================================================================================================
// PROGRAM ROUTINE
// ====================================================================================================================
/**
 * This function runs once and initializes all interfaces, sensors and actors of the implemented datalogger.
 * Also, a headline will be written into the datafile.
 */
void setup() {

    uint8_t errors = 0;

    // Start serial interface
    Serial.begin(9600);
    printInformation("Starting datalogger 'SGUduino 2023/24...");

    display.begin();
    display.write("Hello!");
    display.display();

    // Start temperature sensor
    temperatureSensor.begin();

    // Start IMU
    if(imu.begin()) {
        printSuccess("Init of IMU done successfully.");
    } else {
        printError("Failed to init IMU!");
        errors++;
    }

    // Start altitude sensor
    if(altitudeSensor.begin()) {
        printSuccess("Init of altitude sensor done successfully.");
    } else {
        printError("Failed to init altitude sensor!");
        errors++;
    }

    // SD Storage
    if(storage.begin()) {
        printSuccess("Init of SD storage done successfully.");
    } else {
        printError("Failed to init SD storage!");
        errors++;
    }

    // Neo-pixel RGB LED
    led.begin();
    led.setBrightness(20);
    led.showSuccess();

    if(errors == 0) {
        printSuccess("Datalogger is ready for take-off! See you soon.");
    } else {
        printWarning((String(errors) + " error(s) occurred. Datalogger might not run correctly!").c_str());
    }

    // Write header line of data.
    String header = "";
    header += "Uptime [ms];";
    header += "Temperature [°C];";
    header += "Acceleration X [g];";
    header += "Acceleration Y [g];";
    header += "Acceleration Z [g];";
    header += "Gyroscope X [deg/s];";
    header += "Gyroscope Y [deg/s];";
    header += "Gyroscope Z [deg/s];";
    header += "Pressure [mBar];";
    header += "Altitude [m]";

    if(!storage.store(header)) {
        Serial.println("Failed to write header to SD storage!");
    }
    Serial.println(header);
}


/**
 * This function runs forever.
 * We will use it to implement our sampling and storage routine.
 */
void loop() {

    sensorData.uptimeMillis = millis();

    // Sample temperature sensor
    sensorData.temperature = temperatureSensor.readTemperature();

    // Sample IMU
    imu.readAcceleration(sensorData.accelerationX, sensorData.accelerationY, sensorData.accelerationZ);
    imu.readGyroscope(sensorData.gyroscopeX, sensorData.gyroscopeY, sensorData.gyroscopeZ);

    // Sample altitude sensor
    sensorData.pressure = altitudeSensor.getPressure();
    sensorData.altitude = altitudeSensor.getAltitude();

    // Store data
    storeSensorData();

    // Wait for some time
    delay(1000);
}
